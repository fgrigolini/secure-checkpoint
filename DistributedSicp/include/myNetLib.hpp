#pragma once

#ifndef MYNETLIB_HPP_
#define MYNETLIB_HPP_


#include <fcntl.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h> 
#include <sys/stat.h>
#include <arpa/inet.h> 
#include <errno.h>
#include <thread>
#include <mutex>
#include <stdio.h>
#include <iostream> 
#include <unistd.h> 
#include <cstring>
#include <stdlib.h>
#include <termios.h>
#include <csignal> 


namespace std{
    //##########################################################################
    //######################## < DEFINITIONS > #################################
    //##########################################################################


    #define S_PORT              8080
    #define C_PORT              8081 
    #define BUFFER_DIM          20   
    #define DEVICELIMIT         10
    #define MAXTIMEPASSED       20
    #define SERVERIP            "192.168.137.1"

    #define CLIENT_SNAP_PATH    "../clientSnap/snap.txt"
    #define SERVER_SNAP_PATH    "../serverSnap/snap.txt"
    #define CLIENT_FRAM_PATH    "../clientSnap/fram.txt"
    #define SERVER_FRAM_PATH    "../serverSnap/fram.txt"

    #define msgDataGetId        1   //infomex 
    #define msgDataInteger      2   //infomex + integerByte
    #define msgDataString       3   //infomex + string
    #define msgDataToken        4   //infomex + no data
    #define msgDataRestore      5   //infomex
    #define msgDataAbort        6   //infomex
    #define msgDataDumpCheck    7   //infomex
    #define msgDataSnap1        8   //infomex + key
    #define msgDataSnap2        9   //infomex + key
    #define msgDataRollBack     10  //infomex
    #define msgDataHash         11  //infomex
    #define msgDataMarker       12  //infomex
    #define msgDataGetKeys      13   //infomex
    #define msgDataFHash        14   //infomex
    #define msgDataHashPair     15   //infomex + addr1 + addr2 + val
    #define msgDataNewHash      16   //infomex +data
    #define msgPatternCreate    17
    #define msgDataPattern      18


    //Marker status definition
    #define markerInactive          1   //standard execution
    #define markerReceived          2   //we received the marker
    #define markerLocalStateSaved   3   //we saved the local state, now we send the markers.

    #define HI_NIBBLE(b) (((b) >> 4) & 0x0F)
    #define LO_NIBBLE(b) ((b) & 0x0F)

    //datastruct for my messages. 36Bytes per istanza con buffer_dim=12
    typedef struct datagramInfo{
        uint8_t deviceId;
        uint8_t msgType;
        uint16_t lamportClock;
        char data[BUFFER_DIM-4];
        char msg[2*BUFFER_DIM];

        void clearStrings(){
            memset(msg,0,2*BUFFER_DIM);
            memset(data,0,BUFFER_DIM);
        }

        void getMessage(){
            memset(msg,0,2*BUFFER_DIM);
            msg[0]=0xF0 | HI_NIBBLE(deviceId);
            msg[1]=0xF0 | LO_NIBBLE(deviceId);
            msg[2]=0xF0 | HI_NIBBLE(msgType);
            msg[3]=0xF0 | LO_NIBBLE(msgType);
            char aux=((lamportClock)>>8) & 0x00ff;
            msg[4]= 0xF0 | HI_NIBBLE(aux);
            msg[5]= 0xF0 | LO_NIBBLE(aux);
            aux=(lamportClock) & 0x00ff;
            msg[6]= 0xF0 | HI_NIBBLE(aux);
            msg[7]= 0xF0 | LO_NIBBLE(aux);
            for(int i=0; i<BUFFER_DIM-4; i++){
                msg[8+i*2]=0xF0 | HI_NIBBLE(data[i]);
                msg[8+i*2+1]=0xF0 | LO_NIBBLE(data[i]);
            }
            //msg[2*BUFFER_DIM]=0x00;  
        }

        //occupa 12 byte la stringa
        void toString(unsigned char* s){ 
            s[0]=deviceId;
            s[1]=msgType;
            s[2]=(lamportClock>>8) & 0x00ff;
            s[3]=(lamportClock) & 0x00ff;
            for(int i=0; i<BUFFER_DIM-4; i++){
                s[4+i]=data[i];
            }
        }
        
        void getData(){
            deviceId=(LO_NIBBLE(msg[0])<<4) | (LO_NIBBLE(msg[1]));
            msgType=(LO_NIBBLE(msg[2])<<4) | (LO_NIBBLE(msg[3]));
            lamportClock=(LO_NIBBLE(msg[4])<<12) | (LO_NIBBLE(msg[5])<<8) |(LO_NIBBLE(msg[6])<<4) | (LO_NIBBLE(msg[7]));
            memset(data,0,BUFFER_DIM);
            for(int i=0;i<BUFFER_DIM-4; i++){
                data[i]=(LO_NIBBLE(msg[8+2*i])<<4) | (LO_NIBBLE(msg[8+i*2+1]));
            } 
        }
    } datagramInfo;



    int guard(int n, char * err);


    int readMessage(int* sk, char* b,int bs);


    int startBlockingServer(struct sockaddr_in* sai,uint16_t p);


    int startNonBlockingServer(struct sockaddr_in* sai, uint16_t p);


    int createClient(struct sockaddr_in* cai, char* ip,uint16_t p);


    int connectClient(int* sk,struct sockaddr_in* cai);


}
#endif