cmake_minimum_required(VERSION 3.5)

set(PROJECT "DistributedSicp")
project(${PROJECT})


include(CheckCXXCompilerFlag)

find_package (Threads REQUIRED)


CHECK_CXX_COMPILER_FLAG("-std=c++14" COMPILER_SUPPORTS_CXX14)
if (COMPILER_SUPPORTS_CXX14)
    add_compile_options("-std=c++14")
else()
    message(FATAL_ERROR "Compiler ${CMAKE_CXX_COMPILER} has no C++14 support.")
endif()

# ------------------------------------------------------------------------------

add_compile_options("-O3")
add_compile_options("-g")
add_compile_options("-lpthread")

include_directories(${PROJECT_SOURCE_DIR}/include)

file(GLOB_RECURSE SRCS ${PROJECT_SOURCE_DIR}/src/*.cpp)


add_executable(DistributedSICP ${PROJECT_SOURCE_DIR}/DistributedSicp.cpp ${SRCS})
target_link_libraries (DistributedSICP ${CMAKE_THREAD_LIBS_INIT})

