#ifndef __CC3200R1M1RGC__
// Do not include SPI for CC3200 LaunchPad
#include <SPI.h>
#endif
#include <WiFi.h>
#include <driverlib.h>
#include "distributedCheckpoint.h"

IPAddress serverAddr(192, 168, 137, 1);
WiFiServer LocalServer(C_PORT);
WiFiClient clientSock;

memory dMem;
DatagramInfo di;
memory snap PLACE_IN_FRAM;
bool  restorable PLACE_IN_FRAM;
unsigned char buffer[64];
unsigned long time;

//##################################################################################################################################
//##################################################################################################################################
//##################################################################################################################################
//##################################################################################################################################
//##################################################################################################################################
//##################################################################################################################################

void setup() {
  // put your setup code here, to run once:
  //PM5CTL0 &= ~LOCKLPM5;
  Serial.begin(9600);
  configure_connection();

  pinMode(P1_0,OUTPUT);
  randomSeed(analogRead(P1_0));
  unsigned long mytime = micros();
  
  //ASK SERVER FOR ID
  if(restorable){
    Serial.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    Serial.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    Serial.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    Serial.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    Serial.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    Serial.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    snap.toString(buffer);
    dMem.toData(buffer);
    Serial.print("[#######]> Time: ");
    time = micros()-mytime;
    Serial.println(time);
  }else{
    while(!clientSock.connect(serverAddr, S_PORT)){
      Serial.print(".");
      delay(300);
    }
    dMem.executionStage=1;
    di.deviceId=0;
    di.msgType=msgDataGetId;
    di.lamportClock=0;
    dMem.sendToken=false;
    dMem.markerActive=false;
    di.clearStrings();
    di.getMessage();
    clientSock.print(di.msg);
  
    while(!clientSock.available());
    uint8_t i=0;
    while(clientSock.available()){
        di.msg[i]=(unsigned char)clientSock.read();
        i++;
    }
    di.getData();
    dMem.updateLamportClock(di.lamportClock);
    dMem.localId=di.data[0];
    clientSock.stop();
  }
  
  
  LocalServer.begin();
}



void loop() {
  //Serial.println("ESEGUITO UN CICLO");
  Serial.println(dMem.executionStage,HEX);
  dMem.executionStage=1;
  
  if(dMem.executionStage==1){
    Serial.println("ASCOLTO");
    clientSock=LocalServer.available();
    if(clientSock){
      uint8_t i=0;
      while(clientSock.available()){
        di.msg[i]=(char)clientSock.read();
        i++;
      }
      di.getData();
      dMem.updateLamportClock(di.lamportClock);

      switch(di.msgType){
        case (msgDataToken):{
          Serial.println("Richiesta del Token");
          dMem.executionStage=2;
          break;
        }
        case (msgDataMarker):{
          Serial.println("Ricevuto un MARKER!");
          dMem.executionStage=3;
          break;
        }
        case (msgDataInteger):{
          Serial.println("Ricevuto un Intero!");
          dMem.executionStage=0;
          break;
        }
        default:{
          Serial.println("MESSAGGIO SCONOSCIUTO");
          break;
        }
      }
      //dMem.executionStage=2;
    }else{
      dMem.executionStage=10;
      
    }
      
  }
  
  //listen_P();
  
  //Serial.print(dMem.executionStage,HEX);
  manageToken();
  manageMarker();
  update();
  //dMem.executionStage=20;
  respond();
  
  // put your main code here, to run repeatedly: 
  delay(300);  
}


//##################################################################################################################################
//##################################################################################################################################
//##################################################################################################################################
//##################################################################################################################################
//##################################################################################################################################
//##################################################################################################################################


void listen_P(){
   
}

void manageToken(){
  if(dMem.executionStage==2){
    Serial.println("GESTIONE DEL TOKEN");
    dMem.sendToken=true;
    dMem.localResource++;
    //clientSock.print(di.msg);
    //Serial.println("inviato");
    //clientSock.flush();
    //clientSock.stop();
    dMem.executionStage=10;
  }
}

void manageMarker(){
  if(dMem.executionStage==3){
    Serial.println("GESTIONE DEL MARKER");
    clientSock.stop();
    dMem.markerActive=true;
    
    dMem.toString(buffer);
    snap.toData(buffer);
    restorable=true;
    //uint8_t key[16];
    /*AES256_setCipherKey(AES256_BASE,
                            key,
                            AES256_KEYLENGTH_128BIT);*/
    //MANAGE THE MARKER
    dMem.executionStage=10;
  }
}

void update(){
  if(dMem.executionStage==10){
    dMem.localResource= random(1,10)>6 ? dMem.localResource++: dMem.localResource;
    dMem.executionStage=20;
    }
    
}

void respond(){
  if(dMem.executionStage==20){
    Serial.println("GESTIONE DEGLI INVII");
    if(dMem.markerActive){
      while(!clientSock.connect(serverAddr, S_PORT)){
        Serial.print(".");
        delay(300);
      } 
      di.deviceId=dMem.localId;
      di.msgType=msgDataMarker;
      dMem.updateLamportClock(0);
      di.lamportClock=dMem.localLamportClock;
      di.clearStrings();
      di.getMessage();
      clientSock.print(di.msg);
      clientSock.stop();
      Serial.println("MARKER INVIATO inviato al gateway!");
      Serial.println("Checkpointing FINITO");
      dMem.markerActive=false;
    }

    if(dMem.sendToken){
      while(!clientSock.connect(serverAddr, S_PORT)){
        Serial.print(".");
        delay(300);
      } 
      di.deviceId=dMem.localId;
      di.msgType=msgDataToken;
      dMem.updateLamportClock(0);
      di.lamportClock=dMem.localLamportClock;
      di.clearStrings();
      di.data[0]=dMem.localResource+10;
      di.getMessage();
      clientSock.print(di.msg);
      clientSock.stop();
      Serial.println("Token inviato al gateway!");
      dMem.sendToken=false;
    }
  }
  dMem.executionStage=1;
}




void configure_connection(){
  // your network name also called SSID
  char ssid[] = "grigoNetwork";
  // your network password
  char password[] = "pippo123";

  Serial.print("Attempting to connect to Network named: ");
  // print the network name (SSID);
  Serial.println(ssid); 
  // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
  WiFi.begin(ssid, password);
  while ( WiFi.status() != WL_CONNECTED) {
    // print dots while we wait to connect
    Serial.print(".");
    delay(300);
  }
  
  Serial.println("\nYou're connected to the network");
  Serial.println("Waiting for an ip address");
  
  while (WiFi.localIP() == INADDR_NONE) {
    // print dots while we wait for an ip addresss
    Serial.print(".");
    delay(300);
  }
  
  Serial.println("\nIP Address obtained");

  printWifiData();
  printCurrentNet();
}

void printWifiData() {
  // print your WiFi IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  /*Serial.println(sizeof(ip),HEX);
  uint8_t a=ip & 0x0000000000ff;
  Serial.println(a,HEX);
  a=ip>>8 & 0x0000000000ff;
  Serial.println(a,HEX);
  a=ip>>16 & 0x0000000000ff;
  Serial.println(a,HEX);
  a=ip>>24 & 0x0000000000ff;
  Serial.println(a,HEX);
  a=ip>>32 & 0x0000000000ff;
  Serial.println(a,HEX);
  a=ip>>40 & 0x0000000000ff;
  Serial.println(a,HEX);*/
  
  // print your MAC address:
  byte mac[6];
  WiFi.macAddress(mac);
  Serial.print("MAC address: ");
  Serial.print(mac[5], HEX);
  Serial.print(":");
  Serial.print(mac[4], HEX);
  Serial.print(":");
  Serial.print(mac[3], HEX);
  Serial.print(":");
  Serial.print(mac[2], HEX);
  Serial.print(":");
  Serial.print(mac[1], HEX);
  Serial.print(":");
  Serial.println(mac[0], HEX);

}

void printCurrentNet() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print the MAC address of the router you're attached to:
  byte bssid[6];
  WiFi.BSSID(bssid);
  Serial.print("BSSID: ");
  Serial.print(bssid[5], HEX);
  Serial.print(":");
  Serial.print(bssid[4], HEX);
  Serial.print(":");
  Serial.print(bssid[3], HEX);
  Serial.print(":");
  Serial.print(bssid[2], HEX);
  Serial.print(":");
  Serial.print(bssid[1], HEX);
  Serial.print(":");
  Serial.println(bssid[0], HEX);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.println(rssi);

  // print the encryption type:
  byte encryption = WiFi.encryptionType();
  Serial.print("Encryption Type:");
  Serial.println(encryption, HEX);
  Serial.println();
}

void hash_f(unsigned char *str, unsigned char *res){
    unsigned long long hash = 5381;
    int c;

    while (c = *str++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    for(uint8_t i=0; i<8; i++){
      res[i]=(hash>>(8*i)) & 0x00000000000000ff;
    }
}
