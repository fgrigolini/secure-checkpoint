#include "myNetLib.hpp"

using namespace std;

int std::guard(int n, char * err) { 
    if (n == -1) { 
        perror(err); 
        //exit(1); 
    } 
    return n; 
}

int std::readMessage(int* sk, char* b,int bs){
    int k=read(*sk,b,bs);
    return k; 
}

int std::startBlockingServer(struct sockaddr_in* sai, uint16_t p){
    int opt = 1; 
    int sock=socket(AF_INET,SOCK_STREAM,0);
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));
    
    sai->sin_family = AF_INET; 
    sai->sin_addr.s_addr = INADDR_ANY; 
    sai->sin_port = htons( p );

    bind(sock, (struct sockaddr *)sai, sizeof(*sai));

    return sock;
}

int std::startNonBlockingServer(struct sockaddr_in* sai,uint16_t p){
    int opt = 1; 
    int sock=socket(AF_INET,SOCK_STREAM,0);
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));
    fcntl(sock, F_SETFL, O_NONBLOCK);

    sai->sin_family = AF_INET; 
    sai->sin_addr.s_addr = INADDR_ANY; 
    sai->sin_port = htons( p );

    bind(sock, (struct sockaddr *)sai, sizeof(*sai));

    return sock;
}

int std::createClient(struct sockaddr_in* cai, char* ip, uint16_t p){
    cai->sin_family = AF_INET; 
    cai->sin_port = htons(p);
    int result=socket(AF_INET,SOCK_STREAM,0);
    inet_pton(AF_INET, ip, &(cai->sin_addr));
    return result;
}

int std::connectClient(int* sk,struct sockaddr_in* cai){
    return connect(*sk,(struct sockaddr *)cai, sizeof(*cai));
}
