#include "myNetLib.hpp"
#include <vector>
#include <algorithm>
#include <chrono>
using namespace std;
using namespace chrono;

#define PatternLimit 100
//##########################################################################
//################# < Global Variables & Data Struct > #####################
//##########################################################################
typedef struct sockaddr_in skaddr;

typedef struct remoteCypherInfo {
    unsigned char       newHash[8],oldHash[8],dumpHash[8]; //LH e OH per riprodurre la chiave, NH e OH per controllare che la copia sia integra.
    unsigned char       newKey[16],oldKey[16],dumpKey[16];

    void toString(unsigned char* s){
        //TODO....maybe
    }

} RCI;

typedef struct addrPT {
    uint8_t val;
    uint16_t addr;
} addrPT;

typedef struct memory{
    uint8_t         localId;
    uint8_t         executionStage;
    uint8_t         tokenCounter;
    uint8_t         markerCounter;
    uint8_t         markerTimer;
    uint8_t         sentMarkerCounter;
    uint16_t        localLamportClock;
    datagramInfo    di;
    bool            verification;
    bool            factoryClean;
    bool            markerActive;
    bool            sendMarker;
    uint8_t         deviceCounter;
    uint8_t         deviceList[DEVICELIMIT][8];
    // posizione=ID; [0] contatore; [1-4] ip; [5] marker inviato; [6] marker arrivato; [7] state;
    uint8_t         pendingCounter;
    datagramInfo    pendingList[DEVICELIMIT];
    RCI             cryptInfo;
    
    

    uint16_t size(){
        return (sizeof(uint8_t)*8)+sizeof(uint16_t)+(sizeof(bool)*3)+sizeof(deviceList)+sizeof(di)+sizeof(pendingList);
        /*return  sizeof(localId)+sizeof(localLamportClock)+sizeof(executionStage)+sizeof(tokenCounter)+sizeof(markerCounter)+sizeof(markerTimer)+
                + sizeof(sentMarkerCounter)+sizeof(di)+sizeof(factoryClean)+sizeof(markerActive)+sizeof(sendMarker)+sizeof(deviceCounter)+
                + sizeof(deviceList)+sizeof(pendingCounter)+sizeof(pendingList); */
    } 

    void toString(unsigned char* s ){
        s[0]=localId;
        s[1]=executionStage;
        s[2]=tokenCounter;
        s[3]=markerCounter;
        s[4]=markerTimer;
        s[5]=sentMarkerCounter;
        s[6]=(localLamportClock>>8) & 0x00ff;
        s[7]=(localLamportClock) & 0x00ff;
        s[8]=factoryClean;
        s[9]=markerActive;
        s[10]=sendMarker;
        s[11]=deviceCounter;
        for(int i=0; i<deviceCounter-1; i++){
            s[12+8*i]=deviceList[i][0];
            s[12+8*i+1]=deviceList[i][1];
            s[12+8*i+2]=deviceList[i][2];
            s[12+8*i+3]=deviceList[i][3];
            s[12+8*i+4]=deviceList[i][4];
            s[12+8*i+5]=deviceList[i][5];
            s[12+8*i+6]=deviceList[i][6];
            s[12+8*i+7]=deviceList[i][7];
        }
        s[12+(deviceCounter-1)*8]=pendingCounter;
        unsigned char c[12];
        for(int i=0; i<pendingCounter; i++){
            //unsigned char c[12];
            pendingList[i].toString(c);
            s[13+(deviceCounter-1)*8+i*12]=c[0];
            s[14+(deviceCounter-1)*8+i*12]=c[1];
            s[15+(deviceCounter-1)*8+i*12]=c[2];
            s[16+(deviceCounter-1)*8+i*12]=c[3];
            s[17+(deviceCounter-1)*8+i*12]=c[4];
            s[18+(deviceCounter-1)*8+i*12]=c[5];
            s[19+(deviceCounter-1)*8+i*12]=c[6];
            s[20+(deviceCounter-1)*8+i*12]=c[7];
            s[21+(deviceCounter-1)*8+i*12]=c[8];
            s[22+(deviceCounter-1)*8+i*12]=c[9];
            s[23+(deviceCounter-1)*8+i*12]=c[10];
            s[24+(deviceCounter-1)*8+i*12]=c[11];
        }
        di.toString(c);
        s[13+(deviceCounter-1)*8+(pendingCounter*12)]=c[0];
        s[13+(deviceCounter-1)*8+(pendingCounter*12)+1]=c[1];
        s[13+(deviceCounter-1)*8+(pendingCounter*12)+2]=c[2];
        s[13+(deviceCounter-1)*8+(pendingCounter*12)+3]=c[3];
        s[13+(deviceCounter-1)*8+(pendingCounter*12)+4]=c[4];
        s[13+(deviceCounter-1)*8+(pendingCounter*12)+5]=c[5];
        s[13+(deviceCounter-1)*8+(pendingCounter*12)+6]=c[6];
        s[13+(deviceCounter-1)*8+(pendingCounter*12)+7]=c[7];
        s[13+(deviceCounter-1)*8+(pendingCounter*12)+8]=c[8];
        s[13+(deviceCounter-1)*8+(pendingCounter*12)+9]=c[9];
        s[13+(deviceCounter-1)*8+(pendingCounter*12)+10]=c[10];
        s[13+(deviceCounter-1)*8+(pendingCounter*12)+11]=c[11];
    }

    void toData(unsigned char* s){
        localId=s[0];
        executionStage=s[1];
        tokenCounter=s[2];
        markerCounter=s[3];
        markerTimer=s[4];
        sentMarkerCounter=s[5];
        localLamportClock=(s[6]<<8) | s[7];
        factoryClean=s[8];
        markerActive=s[9];
        sendMarker=s[10];
        deviceCounter=s[11];
        for(int i=0; i<deviceCounter-1; i++){
            deviceList[i][0]=s[12+8*i];
            deviceList[i][1]=s[12+8*i+1];
            deviceList[i][2]=s[12+8*i+2];
            deviceList[i][3]=s[12+8*i+3];
            deviceList[i][4]=s[12+8*i+4];
            deviceList[i][5]=s[12+8*i+5];
            deviceList[i][6]=s[12+8*i+6];
            deviceList[i][7]=s[12+8*i+7];
        }
        pendingCounter=s[12+(deviceCounter-1)*8];
        for(int i=0; i<pendingCounter; i++){
            //unsigned char c[12];
            pendingList[i].deviceId=s[13+(deviceCounter-1)*8+i*12];
            pendingList[i].msgType=s[14+(deviceCounter-1)*8+i*12];
            pendingList[i].lamportClock=((s[15+(deviceCounter-1)*8+i*12])<<8) | (s[16+(deviceCounter-1)*8+i*12]);
            pendingList[i].data[0]=s[17+(deviceCounter-1)*8+i*12];
            pendingList[i].data[1]=s[18+(deviceCounter-1)*8+i*12];
            pendingList[i].data[2]=s[19+(deviceCounter-1)*8+i*12];
            pendingList[i].data[3]=s[20+(deviceCounter-1)*8+i*12];
            pendingList[i].data[4]=s[21+(deviceCounter-1)*8+i*12];
            pendingList[i].data[5]=s[22+(deviceCounter-1)*8+i*12];
            pendingList[i].data[6]=s[23+(deviceCounter-1)*8+i*12];
            pendingList[i].data[7]=s[24+(deviceCounter-1)*8+i*12];
            pendingList[i].getMessage();
        }
        di.deviceId=s[13+(deviceCounter-1)*8+(pendingCounter*12)];
        di.msgType=s[13+(deviceCounter-1)*8+(pendingCounter*12)+1];
        di.lamportClock=(s[13+(deviceCounter-1)*8+(pendingCounter*12)+2]<<8) | (s[13+(deviceCounter-1)*8+(pendingCounter*12)+3]);
        di.data[0]=s[13+(deviceCounter-1)*8+(pendingCounter*12)+4];
        di.data[1]=s[13+(deviceCounter-1)*8+(pendingCounter*12)+5];
        di.data[2]=s[13+(deviceCounter-1)*8+(pendingCounter*12)+6];
        di.data[3]=s[13+(deviceCounter-1)*8+(pendingCounter*12)+7];
        di.data[4]=s[13+(deviceCounter-1)*8+(pendingCounter*12)+8];
        di.data[5]=s[13+(deviceCounter-1)*8+(pendingCounter*12)+9];
        di.data[6]=s[13+(deviceCounter-1)*8+(pendingCounter*12)+10];
        di.data[7]=s[13+(deviceCounter-1)*8+(pendingCounter*12)+11];
        di.getMessage();
    }

    uint16_t sizeString(){
        return 25+((deviceCounter-1)*8+(pendingCounter*12));
    }
    
} memory;

vector<addrPT> patterns(2048);


mutex               mtx;            //to avoid RC
char                buff[16];
int                 clientSock;
int                 serverSock;
int                 addrlen;     
skaddr              local_addr,client_addr;
memory              dMem;
//memory              snap1, snap2;
bool                powered;
datagramInfo        di;
char                ipClient[4];
vector<int>         chosenPattern;
bool                checkable = true;
bool                trustSnap=true;
int                 patternCounter=0;
system_clock::time_point a,b,c,d;
double Total;

void updateLamportClock(uint16_t rlc){
    //printf("<CLOCK> Evoluzione %u >> %u -> %u\n",localLamportClock,rlc,max(rlc,localLamportClock)+1);
    dMem.localLamportClock=max(rlc,dMem.localLamportClock)+1;

}

void listen_procedure();
void manageMarker();
void manageGetId();
void manageToken();
void update();
void beacon();
void sendMarkerProcedure();
void manageSnap1();
void manageSnap2();
void manageHash();
void manageGetKey();
void managePatternCreation();
void manageVerifyPattern();
//######################################################################################################
//######################### < MAIN > ###################################################################
//######################################################################################################



int main(){
    //printf("size %u %u %u\n\n",sizeof(memory),deviceMemory.size(),deviceMemory.sizeString());
    srand(time(NULL));
    powered=true;
    dMem.factoryClean=true;


    uint16_t tempAddr=0x9555;
    for(uint16_t i=0; i<2048; i++){
        patterns[i].addr=tempAddr+i;
    }

    //setup
    dMem.sendMarker=false;
    dMem.markerActive=false;
    dMem.executionStage=0;
    dMem.markerTimer=0;
    dMem.markerCounter=0;
    dMem.deviceCounter=1;
    serverSock=startNonBlockingServer(&local_addr,S_PORT);
    addrlen=sizeof(local_addr);
    listen(serverSock,200);

    while(true){
        /*printf("Nuovo Ciclo\n");

        char ip[16];
        di.msgType=msgDataInteger;
        sprintf(ip,"%u.%u.%u.%u",192,168,137,204);
        clientSock=createClient(&client_addr,ip,C_PORT);
        connectClient(&clientSock,&client_addr);
        di.getMessage();
        printf("<Server %u> Invio Token\n",dMem.localLamportClock);
        send(clientSock,di.msg,BUFFER_DIM*2,0);
        di.clearStrings();
        di.getData();
        //readMessage(&clientSock,di.msg,BUFFER_DIM*2);
        close(clientSock);
        //sleep(2);*/
        dMem.executionStage=0;
        listen_procedure();
        
        manageGetId();
        manageMarker();
        manageGetKey();
        manageSnap2();
        manageHash();
        manageSnap1();
        manageToken();
        managePatternCreation();
        manageVerifyPattern();

        update();
        sendMarkerProcedure();

        //close(clientSock);
        beacon();
        usleep(10000);
    }
    
    return 0;
    
}

void listen_procedure(){
    if(dMem.executionStage==0){
        clientSock=accept(serverSock, (struct sockaddr *)&local_addr, (socklen_t*)&addrlen);
        if(clientSock!=-1){
            //get ip for later use
            

            //Comunicazione ricevuta, apertura deviceMemory.di una nuova connessione
            readMessage(&clientSock,di.msg,BUFFER_DIM*2);
            /*printf("<Server %u> Lettura Messaggio\n",dMem.localLamportClock);
            for(int i=0; i<BUFFER_DIM*2; i++){
                printf("%X ",(unsigned char)di.msg[i]);
            }
            printf("\n");*/
            di.getData();
            updateLamportClock(di.lamportClock);
            ipClient[0]=uint8_t(local_addr.sin_addr.s_addr&0xFF);
            ipClient[1]=uint8_t((local_addr.sin_addr.s_addr&0xFF00)>>8);
            ipClient[2]=uint8_t((local_addr.sin_addr.s_addr&0xFF0000)>>16);
            ipClient[3]=uint8_t((local_addr.sin_addr.s_addr&0xFF000000)>>24);

            //printf("%u %u %s\n",di.deviceId, di.msgType, di.data);
            switch (di.msgType)
            {
            case msgDataGetId:
                {
                    printf("<Server %u> Device %u Vuole entrare nella rete\n",dMem.localLamportClock,di.deviceId);
                    dMem.executionStage=1;
                    break;
                }
            case msgDataMarker:
                {
                    printf("<Server %u> Device %u ha Inviato un Marker\n",dMem.localLamportClock,di.deviceId);
                    dMem.executionStage=2;
                    break;
                }
            case msgDataToken:
                {
                    printf("<Server %u> Device %u ha Inviato un Token\n",dMem.localLamportClock,di.deviceId);
                    dMem.executionStage=3;
                    break;
                }
            case msgDataSnap1:
                {
                    printf("<Server %u> Device %u ha Inviato una nuova chiave\n",dMem.localLamportClock,di.deviceId);
                    dMem.executionStage=4;
                    break;
                }
            case msgDataHash:
                {
                    printf("<Server %u> Device %u ha Inviato un nuovo hash\n",dMem.localLamportClock,di.deviceId);
                    dMem.executionStage=5;
                    break;
                }
            case msgDataGetKeys:
                {
                    printf("<Server %u> Device %u sta richiedendo le chiavi per il restore\n",dMem.localLamportClock,di.deviceId);
                    dMem.executionStage=6;
                    break;
                }
            case msgDataSnap2:
                {
                    printf("<Server %u> Device %u sta richiedendo la vecchia chiave\n",dMem.localLamportClock,di.deviceId);
                    dMem.executionStage=7;
                    break;
                }
            case msgPatternCreate:
                {
                    printf("<Server %u> Device %u sta inviando i risultati dei Pattern\n",dMem.localLamportClock,di.deviceId);
                    dMem.executionStage=8;
                    break;
                }
            case msgDataPattern:
                {
                    printf("<Server %u> Device %u sta inviando i risultati dei Pattern per la verifica\n",dMem.localLamportClock,di.deviceId);
                    dMem.executionStage=9;
                    break;
                }
                
            default:
                {
                    //close(clientSock);
                    break;
                }
                
            }

            
        }else{
            if(errno!=EWOULDBLOCK || errno!=EAGAIN){
                printf("ERROR.\n");
            }else{
                dMem.executionStage=10;
            }
            close(clientSock);
            
        }
        
        
    }
}

void manageGetId(){
    if(dMem.executionStage==1){
        checkable=false;
        patternCounter=0;
        di.deviceId=dMem.localId;
        updateLamportClock(0);
        di.lamportClock=dMem.localLamportClock;
        di.msgType=msgDataPattern;
       
        

        //int k=rand()%2048;
        //printf("<PGK> ");
        for(int i=0; i<8; i++){
            //chosenPattern.push_back(i);
            di.data[i*2]=(patterns[i].addr>>8) & 0x00FF;
            di.data[i*2+1]= patterns[i].addr & 0x00FF;
            //printf("%X ",patterns[i].addr);
        }
        /*printf(" || ");
        for(int i=0; i<16; i++){
            printf("%X ",di.data[i]);
        }
        printf("\n");*/
        di.getMessage();
        int k=send(clientSock,di.msg,BUFFER_DIM*2,0);
        close(clientSock);
        dMem.executionStage=10;
        
        //OLDCODE
        /*di.data[0]=dMem.deviceCounter;
        di.getMessage();
        dMem.deviceList[dMem.deviceCounter-1][1]=ipClient[0];
        dMem.deviceList[dMem.deviceCounter-1][2]=ipClient[1];
        dMem.deviceList[dMem.deviceCounter-1][3]=ipClient[2];
        dMem.deviceList[dMem.deviceCounter-1][4]=ipClient[3];
        dMem.deviceCounter++;*/       
    }   
}

void manageMarker(){
    if(dMem.executionStage==2){
        printf("<Server %u> verifica stato del Checkpoint\n",dMem.localLamportClock);
        close(clientSock);
        if(dMem.deviceList[di.deviceId-1][5]==1 && dMem.deviceList[di.deviceId-1][6]==0){
            dMem.deviceList[di.deviceId-1][6]==1;
            bool i=true;
            for(int k=0; k<dMem.deviceCounter-1; k++){
                if(dMem.deviceList[k][6]==0){
                    int i=false;
                }
            }
            if(i){
                printf("<Server %u> CHECKPOINT COMPLETATO\n",dMem.localLamportClock);
                b=high_resolution_clock::now();
                Total=duration_cast<microseconds>(b - a).count();
                printf("<Server %u> TEMPO PER MARKER: %lf\n",dMem.localLamportClock,Total);
                dMem.markerActive=false;
                dMem.markerCounter=0;
                dMem.markerTimer=0;
                for(int k=0; k<dMem.deviceCounter; k++){
                    dMem.deviceList[k][5]=0;
                    dMem.deviceList[k][6]=0;
                    dMem.markerCounter=0;
                }
            }
        }
        dMem.executionStage=10;
    }
}

void manageToken(){
    if(dMem.executionStage==3){
        close(clientSock);
        printf("<Server %u> Valore Inviato da %i => %X \n",dMem.localLamportClock,di.deviceId,di.data[0]);

        dMem.executionStage=10;
    }
}

void manageSnap1(){
    if(dMem.executionStage==4){
        close(clientSock);
        for(int i=0; i<16; i++){
            dMem.cryptInfo.dumpKey[i]=dMem.cryptInfo.oldKey[i];
            dMem.cryptInfo.oldKey[i]=dMem.cryptInfo.newKey[i];
            dMem.cryptInfo.newKey[i]=di.data[i];
        }
        printf("<Server %u> chiave Memorizzata con successo\n",dMem.localLamportClock);
        dMem.executionStage=10;
    }
}

void manageHash(){
    if(dMem.executionStage==5){
        close(clientSock);
        for(int i=0; i<8; i++){
            dMem.cryptInfo.dumpHash[i]=dMem.cryptInfo.oldHash[i];
            dMem.cryptInfo.oldHash[i]=dMem.cryptInfo.newHash[i];
            dMem.cryptInfo.newHash[i]=di.data[i];
        }
        printf("<Server %u> Hash Memorizzato con successo : ",dMem.localLamportClock);
        for(int i=0; i<8; i++){
            printf("%x ", dMem.cryptInfo.newHash[i]);
        }
        printf("\n");
        dMem.executionStage=10;
    }
}



void manageGetKey(){
    if(dMem.executionStage==6){
        checkable=false;
        patternCounter=0;
        dMem.verification=true;
        c=high_resolution_clock::now();
        //close(clientSock);
        trustSnap=true;
        for(int i=0; i<8; i++){
            //printf("%x ",di.data[i]);
            if(dMem.cryptInfo.newHash[i]!=(unsigned char)di.data[i])
                trustSnap=false;
        }
        //printf("\n");

        di.clearStrings();
        chosenPattern.clear();
        for(int i=0; i<8; i++){
            //int k=rand()%(PatternLimit*8);
            //chosenPattern.push_back(k);
            di.data[i*2]=(patterns[patternCounter*8+i].addr>>8) & 0x00FF;
            di.data[i*2+1]= patterns[patternCounter*8+i].addr & 0x00FF;
        }
        
        di.msgType=msgDataPattern;
        di.deviceId=dMem.localId;
        updateLamportClock(0);
        di.lamportClock=dMem.localLamportClock;
        printf(">>>>> INVIO PATTERN\n");
        di.getMessage();
        send(clientSock,di.msg,BUFFER_DIM*2,0);
        
        close(clientSock);
        dMem.executionStage=10;

        //OLD CODE
        /*
        if(trust){
            di.msgType=msgDataString;
            for(int i=0; i<16; i++){
                di.data[i]=dMem.cryptInfo.newKey[i];
            }
            dMem.verification=false;
            checkable=true;
        }else{
            di.msgType=msgDataSnap2;
        }
        di.getMessage();
        send(clientSock,di.msg,BUFFER_DIM*2,0);

        close(clientSock);
        dMem.executionStage=10;*/
    }
}

void manageSnap2(){
    if(dMem.executionStage==7){
        //close(clientSock);
        bool trust=true;
        for(int i=0; i<8; i++){
            if(dMem.cryptInfo.oldHash[i]!=(unsigned char)di.data[i])
                trust=false;
        }
        di.deviceId=dMem.localId;
        updateLamportClock(0);
        di.lamportClock=dMem.localLamportClock;
        di.clearStrings();
        /*char ip[16];
        sprintf(ip,"%u.%u.%u.%u",(unsigned char)dMem.deviceList[0][1],(unsigned char)dMem.deviceList[0][2],(unsigned char)dMem.deviceList[0][3],(unsigned char)dMem.deviceList[0][4]);
                
        clientSock=createClient(&client_addr,ip,C_PORT);
        connectClient(&clientSock,&client_addr);*/
        if(trust){
            di.msgType=msgDataString;
            for(int i=0; i<16; i++){
                di.data[i]=dMem.cryptInfo.oldKey[i];
            }
            checkable=true;
        }else{
            di.msgType=msgDataRollBack;
        }
        di.getMessage();
        send(clientSock,di.msg,BUFFER_DIM*2,0);
        d=high_resolution_clock::now();

        Total=duration_cast<microseconds>(d - c).count();
        printf("<Server %u> TEMPO PER RESTORE: %lf\n",dMem.localLamportClock,Total);

        close(clientSock);
        dMem.executionStage=10;
        dMem.verification=false;
    }
}


void managePatternCreation(){
    if(dMem.executionStage==8){
        //printf("Pattern ricevuti: ");
        for(uint8_t i=0; i<8; i++){
            patterns[i+patternCounter*8].val=(uint8_t)di.data[i];
            //printf("(%X, %X) ",(uint8_t)di.data[i], patterns[i+patternCounter*8].val);
        }
        //printf("\n");
        patternCounter++;
        di.deviceId=dMem.localId;
        updateLamportClock(0);
        di.lamportClock=dMem.localLamportClock;
        if(patternCounter>PatternLimit*2){
            printf(">>>>> INVIO DELL'ID\n");
            di.msgType=msgDataInteger;
            di.data[0]=dMem.deviceCounter;
            dMem.deviceList[dMem.deviceCounter-1][1]=ipClient[0];
            dMem.deviceList[dMem.deviceCounter-1][2]=ipClient[1];
            dMem.deviceList[dMem.deviceCounter-1][3]=ipClient[2];
            dMem.deviceList[dMem.deviceCounter-1][4]=ipClient[3];
            dMem.deviceCounter++;
            checkable=true;
            patternCounter=0;
        }else{
            //uint8_t g=chosenPattern[7]+1;
            for(uint8_t i=0; i<8; i++){
                di.data[i*2]=(patterns[i+patternCounter*8].addr>>8) & 0x00FF;
                di.data[i*2+1]= patterns[i+patternCounter*8].addr & 0x00FF;
            }
            di.msgType=msgDataPattern;
            printf(">>>>> INVIO PATTERN\n");
        }
        di.getMessage();

        int k=send(clientSock,di.msg,BUFFER_DIM*2,0);
        close(clientSock);
        dMem.executionStage=10;
    }
}

void manageVerifyPattern(){
    if(dMem.executionStage==9){
        bool tampered=false;
        for(int i=0; i<8; i++){
            //printf(" [%X %X] ",(uint8_t)di.data[i],patterns[i+patternCounter*8].val);
            if((uint8_t)di.data[i]!=patterns[i+patternCounter*8].val){
                //printf("!");
                tampered=true;
            }
        }
        //printf("\n");
        patternCounter++;
        di.clearStrings();
        di.deviceId=dMem.localId;
        updateLamportClock(0);
        di.lamportClock=dMem.localLamportClock;
        di.clearStrings();
        if(tampered){
            printf("TROVATO BYTE ERRATO!!!!\n");
            di.msgType=msgDataRollBack;
            checkable=true;
            dMem.deviceCounter--;
        }else{
            if(patternCounter>PatternLimit){
                if(trustSnap){
                    di.msgType=msgDataString;
                    for(int i=0; i<16; i++){
                        di.data[i]=dMem.cryptInfo.newKey[i];
                    }
                    d=high_resolution_clock::now();
                    Total=duration_cast<microseconds>(d - c).count();
                    printf("<Server %u> TEMPO PER RESTORE: %lf\n",dMem.localLamportClock,Total);
                }else{
                    di.msgType=msgDataSnap2;
                
                }
            }else{
                chosenPattern.clear();
                for(int i=0; i<8; i++){
                    //int k=rand()%(PatternLimit*8);
                    di.data[i*2]=(patterns[i+patternCounter*8].addr>>8) & 0x00FF;
                    di.data[i*2+1]= patterns[i+patternCounter*8].addr & 0x00FF;
                }
                di.msgType=msgDataPattern;
            }
        }

        di.getMessage();
        int k=send(clientSock,di.msg,BUFFER_DIM*2,0);
        close(clientSock);
        dMem.executionStage=10;
    }
}


void update(){
    if(dMem.executionStage==10){
        //printf("<Server %u> Update Locale\n",dMem.localLamportClock);
        dMem.markerTimer++;
        
        if(dMem.markerTimer>=150 && checkable && !dMem.markerActive){
            printf("<Server %u> CHECKPOINT INIZIATO\n",dMem.localLamportClock);
            dMem.markerActive=true;
            dMem.sendMarker=true;
            a=high_resolution_clock::now();
        }
        for(int i=0; i<dMem.deviceCounter-1; i++){
            if(dMem.deviceList[i][0]>8){
                //send request
            }else{
                dMem.deviceList[i][0]++;
            }
        }
        dMem.executionStage=1;
    }
    
}

void sendMarkerProcedure(){
    if(dMem.sendMarker && !dMem.verification){
        for(int i=0; i<dMem.deviceCounter-1; i++){
            if(dMem.deviceList[i][5]==0){
                printf("<Server %u> Invio Marker a %i\n",dMem.localLamportClock,i+1);
                dMem.deviceList[i][5]=1;
                char ip[16];
                sprintf(ip,"%u.%u.%u.%u",(unsigned char)dMem.deviceList[0][1],(unsigned char)dMem.deviceList[0][2],(unsigned char)dMem.deviceList[0][3],(unsigned char)dMem.deviceList[0][4]);
                clientSock=createClient(&client_addr,ip,C_PORT);
                connectClient(&clientSock,&client_addr);

                di.deviceId=dMem.localId;
                di.msgType=msgDataMarker;
                updateLamportClock(0);
                di.lamportClock=dMem.localLamportClock;
                di.clearStrings();
                di.getMessage();
                send(clientSock,di.msg,BUFFER_DIM*2,0);
                close(clientSock);
                dMem.markerCounter++;
            }
        }
        if(dMem.markerCounter==dMem.deviceCounter-1){
            printf("<Server %u> Tutti i marker sono stati inviati\n",dMem.localLamportClock);
            dMem.sendMarker=false;
        }

    }
}

void beacon(){
    if(dMem.deviceCounter>1 && (rand()%10)>8 && !dMem.verification){
        di.deviceId=60;
        di.msgType=msgDataToken;
        updateLamportClock(0);
        di.lamportClock=dMem.localLamportClock;
        di.clearStrings();
        di.data[0]='B';
        di.data[1]='E';
        di.data[2]='A';
        di.data[3]='C';
        di.data[4]='O';
        di.data[5]='N';
        di.data[6]='!';
        di.getMessage();

        char ip[16];
        sprintf(ip,"%u.%u.%u.%u",(unsigned char)dMem.deviceList[0][1],(unsigned char)dMem.deviceList[0][2],(unsigned char)dMem.deviceList[0][3],(unsigned char)dMem.deviceList[0][4]);
        clientSock=createClient(&client_addr,ip,C_PORT);
        connectClient(&clientSock,&client_addr);
        send(clientSock,di.msg,BUFFER_DIM*2,0);
        di.clearStrings();
        //readMessage(&clientSock,di.msg,BUFFER_DIM*2);
        di.getData();
        printf("<Server %u> Invio Token\n",dMem.localLamportClock);
        //updateLamportClock(di.lamportClock);
        //printf("<Server %u> Dato ricevuto da %u : %X\n",dMem.localLamportClock,di.deviceId, (unsigned char) di.data[0]);
        close(clientSock);

    }
    

}