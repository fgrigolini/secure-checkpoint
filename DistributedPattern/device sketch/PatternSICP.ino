#ifndef __CC3200R1M1RGC__
// Do not include SPI for CC3200 LaunchPad
#include <SPI.h>
#endif
#include <WiFi.h>
#include <driverlib.h>
#include "distributedCheckpoint.h"

IPAddress serverAddr(192, 168, 137, 1);
WiFiServer LocalServer(C_PORT);
WiFiClient clientSock;

memory dMem;
DatagramInfo di;
bool  restorable PLACE_IN_FRAM;
unsigned char buffer[64];
uint8_t key[16];
uint8_t snap1[16] PLACE_IN_FRAM;
uint8_t snap2[16] PLACE_IN_FRAM;
uint8_t dumpSnap[16] PLACE_IN_FRAM;
 

//##################################################################################################################################
//##################################################################################################################################
//##################################################################################################################################
//##################################################################################################################################
//##################################################################################################################################
//##################################################################################################################################

void setup() {
  // put your setup code here, to run once:
  //PM5CTL0 &= ~LOCKLPM5;
  Serial.begin(9600);
  configure_connection();

  /*valKal=0xDA;
  uint8_t * point1=&valKal;
  uint8_t * point=(uint8_t *)0x4515;
  uint8_t c=*point;
  Serial.println(c,HEX);
  c=*point1;
  Serial.println(c,HEX);
  Serial.println();
  uint16_t a=(uint16_t)&snap1;
  Serial.println(a,HEX);
  a=(uint16_t)&snap1+15;
  Serial.println(a,HEX);
  a=(uint16_t)&snap2;
  Serial.println(a,HEX);
  a=(uint16_t)&snap2+15;
  Serial.println(a,HEX);
  a=(uint16_t)&dumpSnap;
  Serial.println(a,HEX);
  a=(uint16_t)&dumpSnap+15;
  Serial.println(a,HEX);
  
  while(true){}*/
  
  pinMode(P1_0,OUTPUT);
  randomSeed(analogRead(A0));
  
  
  //ASK SERVER FOR ID
  
  if(restorable){
    // ###################################################################################################### GET KEY
    Serial.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    Serial.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    Serial.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    Serial.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    Serial.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    Serial.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    
    while(!clientSock.connect(serverAddr, S_PORT)){
      Serial.print(".");
      delay(300);
    }
    
    uint8_t data[16];
    di.deviceId=0;
    di.lamportClock=0;
    di.msgType=msgDataGetKeys;
    hash_f(snap1,data);
    di.clearStrings();
    for(uint8_t i=0; i<8; i++){
      di.data[i]=data[i];
    }
    di.getMessage();
    clientSock.print(di.msg); // HASH INVIATO
    di.clearStrings();
    while(!clientSock.available());
    uint8_t i=0;
    while(clientSock.available()){
        di.msg[i]=(unsigned char)clientSock.read();
        i++;
    }
    clientSock.stop();
    di.getData();

    bool goon=true;

    while(goon){
        uint8_t sel[8]; 
        for(i=0; i<8; i++){ // PRENDO GLI INDIRIZZI e NE TROVO I RISPETTIVI VALORI
            uint8_t* pt= (uint8_t*)((di.data[2*i]<<8)+di.data[2*i+1]);
            //Serial.print((uint)(uint8_t*)(di.data[2*i]<<8)+di.data[2*i+1],HEX);
            //Serial.print(" ");
            sel[i]=*pt;
        }
        //Serial.println("");
        di.clearStrings();
        di.deviceId=dMem.localId;
        di.lamportClock=dMem.localLamportClock;
        di.msgType=msgDataPattern;
        for(uint8_t i=0; i<8; i++){
          di.data[i]=sel[i]; // CARICO I VALORI IN DI.DATA
        } 
        di.getMessage();
        while(!clientSock.connect(serverAddr, S_PORT)){
          Serial.print(".");
          delay(300);
        }
        clientSock.print(di.msg);    // INVIO I VALORI AL GATEWAY
        
        while(!clientSock.available());
        i=0;
        while(clientSock.available()){
          di.msg[i]=(unsigned char)clientSock.read(); // RECUPERO LA RISPOSTA DEL GATE
          i++;
        }
        di.getData();
        clientSock.stop();
        if(di.msgType==msgDataRollBack){
            Serial.println("ATTENZIONE !! ATTENZIONE!! Necessario reset di fabbrica!");
            
            while(true){}
        }
        if(di.msgType!=msgDataPattern){
            goon=false;
        }  
    }
    
    if(di.msgType==msgDataSnap2){
          //INVIAMO IL SECONDO HASH
          di.deviceId=0;
          di.lamportClock=0;
          di.msgType=msgDataSnap2;
          hash_f(snap2,data);
          di.clearStrings();
          for(uint8_t i=0; i<8; i++){
            di.data[i]=data[i];
          }
          di.getMessage();
          while(!clientSock.connect(serverAddr, S_PORT)){
            Serial.print(".");
            delay(300);
          }
          clientSock.print(di.msg);
          di.clearStrings();
          while(!clientSock.available());
          uint8_t i=0;
          while(clientSock.available()){
              di.msg[i]=(unsigned char)clientSock.read();
              i++;
          }
          clientSock.stop();
          di.getData();
          if(di.msgType==msgDataRollBack){
              Serial.println("ATTENZIONE !! ATTENZIONE!! Necessario reset di fabbrica!");
              while(true){}
          }else{
              Serial.println("RICEVUTA LA CHIAVE PER IL SECONDO CHECKPOINT");
              for(uint8_t i=0; i<16; i++){
                 key[i]=di.data[i];
                 data[i]=snap2[i];
              }
          }
    }else{
      Serial.println("RICEVUTA LA CHIAVE PER IL PRIMO CHECKPOINT");
      for(uint8_t i=0; i<16; i++){
         key[i]=di.data[i];
         data[i]=snap1[i];
      }
    }

    
      
      AES256_setDecipherKey(AES256_BASE, key, AES256_KEYLENGTH_128BIT);
      //dMem.toString(buffer);
      AES256_decryptData(AES256_BASE, data,buffer);
      dMem.toData(buffer);

      Serial.println("Stato Decryptato con Successo, Esecuzione riprende");
    
    
    //snap.toString(buffer);
    //dMem.toData(buffer);
  }else{
    //########################################################################### GET ID
    while(!clientSock.connect(serverAddr, S_PORT)){
      Serial.print(".");
    }
    di.clearStrings();
    dMem.executionStage=1;
    di.deviceId=0;
    di.msgType=msgDataGetId;
    di.lamportClock=0;
    dMem.sendToken=false;
    dMem.markerActive=false;
    di.getMessage();
    clientSock.print(di.msg); //INVIO RICHIESTA ID
    di.clearStrings();
    bool goon=true;
    
    
    while(!clientSock.available());
    uint8_t i=0;
    while(clientSock.available()){
        di.msg[i]=(unsigned char)clientSock.read();
        i++;
    }
    di.getData();
    clientSock.stop();
    
    
    while(goon){
       uint8_t sel[8]; 
       for(uint8_t i=0; i<8; i++){ // PRENDO I VALORI 
          uint8_t* pt= (uint8_t*)(di.data[2*i]<<8)+di.data[2*i+1];
          //Serial.print((di.data[2*i]<<8)+di.data[2*i+1],HEX);
          //Serial.print(" ");
          sel[i]=*pt;
       }
       //Serial.println(); 
       
       di.clearStrings();
       di.deviceId=dMem.localId;
       di.lamportClock=dMem.localLamportClock;
       di.msgType=msgPatternCreate;
       for(uint8_t i=0; i<8; i++){
          di.data[i]=sel[i]; // CARICO I VALORI IN DI.DATA
       } 
       di.getMessage();
       
      while(!clientSock.connect(serverAddr, S_PORT)){
        Serial.print(".");
        delay(300);
      }
      clientSock.print(di.msg);
          
      di.clearStrings();

      while(!clientSock.available());
      uint8_t i=0;
      while(clientSock.available()){
          di.msg[i]=(unsigned char)clientSock.read();
          i++;
      }
      di.getData();
      if(di.msgType==msgDataInteger){
        goon=false;
      }
      clientSock.stop();
   }
    
    
    di.getData();
    dMem.updateLamportClock(di.lamportClock);
    dMem.localId=di.data[0];
    clientSock.stop();
  }
  
  
  LocalServer.begin();
}



void loop() {
  //Serial.println("ESEGUITO UN CICLO");
  Serial.println(dMem.executionStage,HEX);
  dMem.executionStage=1;
  
  if(dMem.executionStage==1){
    Serial.println("ASCOLTO");
    clientSock=LocalServer.available();
    if(clientSock){
      uint8_t i=0;
      while(clientSock.available()){
        di.msg[i]=(char)clientSock.read();
        i++;
      }
      di.getData();
      dMem.updateLamportClock(di.lamportClock);

      switch(di.msgType){
        case (msgDataToken):{
          Serial.println("Richiesta del Token");
          dMem.executionStage=2;
          break;
        }
        case (msgDataMarker):{
          Serial.println("Ricevuto un MARKER!");
          dMem.executionStage=3;
          break;
        }
        case (msgDataInteger):{
          Serial.println("Ricevuto un Intero!");
          dMem.executionStage=0;
          break;
        }
        default:{
          Serial.println("MESSAGGIO SCONOSCIUTO");
          break;
        }
      }
      //dMem.executionStage=2;
    }else{
      dMem.executionStage=10;
      
    }
      
  }
  
  //listen_P();
  
  //Serial.print(dMem.executionStage,HEX);
  manageToken();
  manageMarker();
  update();
  //dMem.executionStage=20;
  respond();
  
  // put your main code here, to run repeatedly: 
  delay(1000);  
}


//##################################################################################################################################
//##################################################################################################################################
//##################################################################################################################################
//##################################################################################################################################
//##################################################################################################################################
//##################################################################################################################################


void listen_P(){
   
}

void manageToken(){
  if(dMem.executionStage==2){
    Serial.println("GESTIONE DEL TOKEN");
    dMem.sendToken=true;
    dMem.localResource++;
    //clientSock.print(di.msg);
    //Serial.println("inviato");
    //clientSock.flush();
    //clientSock.stop();
    dMem.executionStage=10;
  }
}

void manageMarker(){
  if(dMem.executionStage==3){
    Serial.println("GESTIONE DEL MARKER");
    clientSock.stop();
    uint8_t data[16];
    dMem.markerActive=true;
    
    
    //snap.toData(buffer);
    restorable=true;
    
    for(uint8_t i=0; i<16; i++){
      key[i]=random(0,255);
    }
    
    AES256_setCipherKey(AES256_BASE,
                            key,
                            AES256_KEYLENGTH_128BIT);
    dMem.toString(buffer);
    AES256_encryptData(AES256_BASE,
                            buffer,
                            data);
    for(uint8_t i=0; i<16; i++){
      dumpSnap[i]=snap2[i];
      snap2[i]=snap1[i];
      snap1[i]=data[i];
    }
    hash_f(snap1,buffer);
    //Key contiene la chiave randomica e buffer l'hash dello snap.

    dMem.executionStage=10;
  }
}

void update(){
  if(dMem.executionStage==10){
    dMem.localResource= random(1,10)>6 ? dMem.localResource++: dMem.localResource;
    dMem.executionStage=20;
    }
    
}

void respond(){
  if(dMem.executionStage==20){
    Serial.println("GESTIONE DEGLI INVII");
    if(dMem.markerActive){
      while(!clientSock.connect(serverAddr, S_PORT)){
        Serial.print(".");
        delay(300);
      } 
      di.deviceId=dMem.localId;
      di.msgType=msgDataMarker;
      dMem.updateLamportClock(0);
      di.lamportClock=dMem.localLamportClock;
      di.clearStrings();
      di.getMessage();
      clientSock.print(di.msg);
      clientSock.stop();
      Serial.println("MARKER INVIATO inviato al gateway!");
      Serial.println("Checkpointing FINITO");
      dMem.markerActive=false;
      //INVIO LA CHIAVE
      while(!clientSock.connect(serverAddr, S_PORT)){
        Serial.print(".");
        delay(300);
      } 
      di.deviceId=dMem.localId;
      di.msgType=msgDataSnap1;
      dMem.updateLamportClock(0);
      di.lamportClock=dMem.localLamportClock;
      di.clearStrings();
      for(uint8_t i=0; i<16; i++){
        di.data[i]=key[i];
      }
      di.getMessage();
      clientSock.print(di.msg);
      clientSock.stop();
      Serial.println("Chiave Randomica Inviata al gateway!");
      
      //INVIO L'HASH
      while(!clientSock.connect(serverAddr, S_PORT)){
        Serial.print(".");
        delay(300);
      } 
      di.deviceId=dMem.localId;
      di.msgType=msgDataHash;
      dMem.updateLamportClock(0);
      di.lamportClock=dMem.localLamportClock;
      di.clearStrings();
      for(uint8_t i=0; i<8; i++){
        di.data[i]=buffer[i];
      }
      di.getMessage();
      clientSock.print(di.msg);
      clientSock.stop();
      Serial.println("Chiave Randomica Inviata al gateway!");
    }

    if(dMem.sendToken){
      while(!clientSock.connect(serverAddr, S_PORT)){
        Serial.print(".");
        delay(300);
      } 
      di.deviceId=dMem.localId;
      di.msgType=msgDataToken;
      dMem.updateLamportClock(0);
      di.lamportClock=dMem.localLamportClock;
      di.clearStrings();
      di.data[0]=dMem.localResource+10;
      di.getMessage();
      clientSock.print(di.msg);
      clientSock.stop();
      Serial.println("Token inviato al gateway!");
      dMem.sendToken=false;
    }
  }
  dMem.executionStage=1;
}




void configure_connection(){
  // your network name also called SSID
  char ssid[] = "grigoNetwork";
  // your network password
  char password[] = "pippo123";

  Serial.print("Attempting to connect to Network named: ");
  // print the network name (SSID);
  Serial.println(ssid); 
  // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
  WiFi.begin(ssid, password);
  while ( WiFi.status() != WL_CONNECTED) {
    // print dots while we wait to connect
    Serial.print(".");
    delay(300);
  }
  
  Serial.println("\nYou're connected to the network");
  Serial.println("Waiting for an ip address");
  
  while (WiFi.localIP() == INADDR_NONE) {
    // print dots while we wait for an ip addresss
    Serial.print(".");
    delay(300);
  }
  
  Serial.println("\nIP Address obtained");

  printWifiData();
  printCurrentNet();
}

void printWifiData() {
  // print your WiFi IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  /*Serial.println(sizeof(ip),HEX);
  uint8_t a=ip & 0x0000000000ff;
  Serial.println(a,HEX);
  a=ip>>8 & 0x0000000000ff;
  Serial.println(a,HEX);
  a=ip>>16 & 0x0000000000ff;
  Serial.println(a,HEX);
  a=ip>>24 & 0x0000000000ff;
  Serial.println(a,HEX);
  a=ip>>32 & 0x0000000000ff;
  Serial.println(a,HEX);
  a=ip>>40 & 0x0000000000ff;
  Serial.println(a,HEX);*/
  
  // print your MAC address:
  byte mac[6];
  WiFi.macAddress(mac);
  Serial.print("MAC address: ");
  Serial.print(mac[5], HEX);
  Serial.print(":");
  Serial.print(mac[4], HEX);
  Serial.print(":");
  Serial.print(mac[3], HEX);
  Serial.print(":");
  Serial.print(mac[2], HEX);
  Serial.print(":");
  Serial.print(mac[1], HEX);
  Serial.print(":");
  Serial.println(mac[0], HEX);

}

void printCurrentNet() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print the MAC address of the router you're attached to:
  byte bssid[6];
  WiFi.BSSID(bssid);
  Serial.print("BSSID: ");
  Serial.print(bssid[5], HEX);
  Serial.print(":");
  Serial.print(bssid[4], HEX);
  Serial.print(":");
  Serial.print(bssid[3], HEX);
  Serial.print(":");
  Serial.print(bssid[2], HEX);
  Serial.print(":");
  Serial.print(bssid[1], HEX);
  Serial.print(":");
  Serial.println(bssid[0], HEX);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.println(rssi);

  // print the encryption type:
  byte encryption = WiFi.encryptionType();
  Serial.print("Encryption Type:");
  Serial.println(encryption, HEX);
  Serial.println();
}

void hash_f(unsigned char *str, unsigned char *res){
    unsigned long long hash = 5381;
    int c;

    while (c = *str++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    for(uint8_t i=0; i<8; i++){
      res[i]=(hash>>(8*i)) & 0x00000000000000ff;
    }
}
