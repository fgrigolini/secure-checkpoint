#ifndef _DistributedCheckpoint_H_
#define _DistributedCheckpoint_H_

#include <stdio.h>
#include "Energia.h"

//##########################################################################
//######################## < DEFINITIONS > #################################
//##########################################################################


#define S_PORT              8080
#define C_PORT              8081 
#define BUFFER_DIM          20   
#define DEVICELIMIT         10
#define MAXTIMEPASSED       20
#define SERVERIP            "192.168.137.1"


//Message types definitions


#define msgDataGetId        1   //infomex 
#define msgDataInteger      2   //infomex + integerByte
#define msgDataString       3   //infomex + string
#define msgDataToken        4   //infomex + no data
#define msgDataRestore      5   //infomex
#define msgDataAbort        6   //infomex
#define msgDataDumpCheck    7   //infomex
#define msgDataSnap1        8   //infomex + key
#define msgDataSnap2        9   //infomex + key
#define msgDataRollBack     10  //infomex
#define msgDataHash         11  //infomex
#define msgDataMarker       12  //infomex
#define msgDataGetKeys      13   //infomex
#define msgDataFHash        14   //infomex
#define msgDataHashPair     15   //infomex
#define msgDataNewHash      16   //infomex +data
#define msgPatternCreate    17
#define msgDataPattern      18

//Marker status definition
#define markerInactive          1   //standard execution
#define markerReceived          2   //we received the marker
#define markerLocalStateSaved   3   //we saved the local state, now we send the markers.

#define HI_NIBBLE(b) (((b) >> 4) & 0x0F)
#define LO_NIBBLE(b) ((b) & 0x0F)



typedef struct datagramInfo{
    uint8_t deviceId;
    uint8_t msgType;
    uint16_t lamportClock;
    char data[BUFFER_DIM-4];
    char msg[2*BUFFER_DIM];
    
    void clearStrings(){
        memset(msg,0,2*BUFFER_DIM);
        memset(data,0,BUFFER_DIM);
    }

    void clean(){
        deviceId=0;
        msgType=0;
        lamportClock=0;
        clearStrings();
    }

    void getMessage(){
        memset(msg,0,2*BUFFER_DIM);
        msg[0]=0xF0 | HI_NIBBLE(deviceId);
        msg[1]=0xF0 | LO_NIBBLE(deviceId);
        msg[2]=0xF0 | HI_NIBBLE(msgType);
        msg[3]=0xF0 | LO_NIBBLE(msgType);
        char aux=((lamportClock)>>8) & 0x00ff;
        msg[4]= 0xF0 | HI_NIBBLE(aux);
        msg[5]= 0xF0 | LO_NIBBLE(aux);
        aux=(lamportClock) & 0x00ff;
        msg[6]= 0xF0 | HI_NIBBLE(aux);
        msg[7]= 0xF0 | LO_NIBBLE(aux);
        for(int i=0; i<BUFFER_DIM-4; i++){
            msg[8+i*2]=0xF0 | HI_NIBBLE(data[i]);
            msg[8+i*2+1]=0xF0 | LO_NIBBLE(data[i]);
        }
        /*Serial.print("INVIO DI: ");
        for(uint8_t i=0; i<2*BUFFER_DIM; i++){
          Serial.print((uint8_t)msg[i],HEX);
          Serial.print(" "); 
        }
        Serial.print("\n");*/
        //msg[2*BUFFER_DIM]=0x00;  
    }

    void toString(unsigned char* s){ 
        s[0]=deviceId;
        s[1]=msgType;
        s[2]=(lamportClock>>8) & 0x00ff;
        s[3]=(lamportClock) & 0x00ff;
        for(int i=0; i<BUFFER_DIM-4; i++){
            s[4+i]=data[i];
        }
    }
    
    void getData(){
        /*Serial.print("RICEZIONE DI: ");
        for(uint8_t i=0; i<2*BUFFER_DIM; i++){
          Serial.print((uint8_t)msg[i],HEX);
          Serial.print(" "); 
        }
        Serial.print("\n");*/
        deviceId=(LO_NIBBLE(msg[0])<<4) | (LO_NIBBLE(msg[1]));
        msgType=(LO_NIBBLE(msg[2])<<4) | (LO_NIBBLE(msg[3]));
        lamportClock=(LO_NIBBLE(msg[4])<<12) | (LO_NIBBLE(msg[5])<<8) |(LO_NIBBLE(msg[6])<<4) | (LO_NIBBLE(msg[7]));
        memset(data,0,BUFFER_DIM);
        for(int i=0;i<BUFFER_DIM-4; i++){
            data[i]=(LO_NIBBLE(msg[8+2*i])<<4) | (LO_NIBBLE(msg[8+i*2+1]));
        } 
    }
} DatagramInfo;



typedef struct memory{
    uint8_t         localId;
    uint8_t         tokenCounter;
    uint8_t         executionStage;
    uint8_t         localResource;
    uint16_t        localLamportClock;
    bool            factoryClean;
    bool            markerActive;
    bool            tokenActive;
    bool            sendToken;
    bool            verification;

    void clean(){
      localId=0;
      executionStage=0;
      tokenCounter=0;
      localResource=0;
      localLamportClock=0;
      factoryClean=false;
      markerActive=false;
      tokenActive=false;
      verification=false;
      sendToken=false;
    }
    
    uint16_t size(){
        return sizeof(uint8_t)*4+sizeof(bool)*4+sizeof(uint16_t);
    }

    void toString(unsigned char* s ){
        s[0]=localId;
        s[1]=executionStage;
        s[2]=tokenCounter;
        s[3]=localResource;
        s[4]=(localLamportClock>>8) & 0x00ff;
        s[5]=(localLamportClock) & 0x00ff;
        s[6]=factoryClean;
        s[7]=markerActive;
        s[8]=tokenActive;
        s[9]=sendToken;
        s[10]=verification;
    }

    void toData(unsigned char * s){
        localId=s[0];
        executionStage=s[1];
        tokenCounter=s[2];
        localResource=s[3];
        localLamportClock=(s[4]<<8) | s[5];
        factoryClean=s[6];
        markerActive=s[7];
        tokenActive=s[8];
        sendToken=s[9];
        verification=s[10];
    }

    unsigned int sizeString(){
        return 10+BUFFER_DIM;
    }

void updateLamportClock(uint16_t rlc){
    localLamportClock=max(rlc,localLamportClock)+1;
    /*Serial.println("################################################");
    Serial.println(localLamportClock,HEX);
    Serial.println("################################################");*/
}
} LocalMemory;




#endif
